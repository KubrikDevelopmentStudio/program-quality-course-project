package com.kcd67;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GraphTest {

    @Test()
    void NotValidEdgesNumber() {
        Graph graph = new Graph();
        Assertions.assertThrows(NotValidEdgesNumber.class, () -> {
            graph.createUndirectedGraph(new ArrayList<>(), 1,2);
        });
    }

    @Test()
    void NotValidVerticesNumber() {
        List<Edge> edges = Arrays.asList(
                new Edge(0, 1, 3),
                new Edge(0, 4, 1)
                );
        Graph graph = new Graph();
        Assertions.assertThrows(NotValidVerticesNumber.class, () -> {
            graph.createUndirectedGraph(edges, 1,-1);
        });
    }

    @Test
    void createUndirectedGraph() throws NotValidEdgesNumber, NotValidVerticesNumber {
        List<Edge> edges = Arrays.asList(
                new Edge(0, 1, 3),
                new Edge(0, 4, 1)
        );
        Graph graph = new Graph();
        Assertions.assertTrue(graph.createUndirectedGraph(edges, 1, 2));
    }
}